import LogoScene from "../scenes/LogoScene";

export default class Mario extends Phaser.GameObjects.Sprite {
    
    public scene: Phaser.Scene;

    public body: Phaser.Physics.Arcade.Body;

    public keys: Phaser.Types.Input.Keyboard.CursorKeys;

    public anims: Phaser.Animations.AnimationState;

    constructor(scene: LogoScene, x:number, y: number, texture: string, frame: number){
        super(scene, x,y, texture, frame)
        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);
        this.body.setCollideWorldBounds(true).setSize(12, 16, true);
        this.keys = this.scene.input.keyboard.createCursorKeys();
        this.anims.create({
            key: 'walk',
            frames: this.anims.generateFrameNumbers('mario', { frames: [ 0, 1, 2, 3 ] }),
            frameRate: 8,
            repeat: -1
        });
    }
    
    public preUpdate(time: number, delta: number): void {
        super.preUpdate(time, delta);
        

        if(this.keys.left.isDown && this.keys.right.isUp){
            this.body.setVelocityX(-100);
            
        }
        if(this.keys.left.isUp && this.keys.down.isUp){
            this.body.setVelocityX(0);
            this.anims.pause();
        }
        if(this.keys.right.isDown && this.keys.left.isUp){
            this.body.setVelocityX(100)
        }
        if(this.keys.right.isDown || this.keys.left.isDown){
            this.anims.play('walk', true);
        }
        if(this.keys.space.isDown){
            this.body.setVelocityY(-100)
        }
    }

}