import LogoScene from "../scenes/LogoScene";

export default class Goomba extends Phaser.GameObjects.Sprite{
    public scene: Phaser.Scene;

    public body: Phaser.Physics.Arcade.Body;

    public anims: Phaser.Animations.AnimationState;

    constructor(scene: LogoScene, x:number, y: number, texture: string, frame: number){
        super(scene, x,y, texture, frame)
        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);
        this.body.setCollideWorldBounds(true).setSize(12, 16, true);
        this.anims.create({
            key: 'walk',
            frames: this.anims.generateFrameNumbers('mario', { frames: [ 11, 12] }),
            frameRate: 2,
            repeat: -1
        });

    }
    public preUpdate(time: number, delta: number): void {
        super.preUpdate(time, delta);
        this.body.setVelocityX(-50);
        this.anims.play('walk', true)
    }
}