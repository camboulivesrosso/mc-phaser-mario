import 'phaser';
import { WIDTH, HEIGHT, GAME_TITLE, GRAVITY } from './constant/config';

import LogoScene from './scenes/LogoScene';

const config: Phaser.Types.Core.GameConfig = {
    type: Phaser.WEBGL,
    width: 360,
    height: 160,
    pixelArt: true,
    scale: {
        autoCenter: Phaser.Scale.Center.CENTER_HORIZONTALLY,
        mode: Phaser.Scale.ScaleModes.FIT,
        autoRound: true,
    },
    physics: {
        default: 'arcade',
        arcade: {
            tileBias: 10,
            gravity: {
                y: GRAVITY
            },
            debug: false,
            debugShowBody: true,
            debugShowStaticBody: true,
        },
    },
    scene: [
        LogoScene,
    ]
};

const game = new Phaser.Game(config);
