import { Scene } from 'phaser';
// import needed assets files
import logo from '../assets/graphics/logo.png';
import { HEIGHT, WIDTH } from '../constant/config';
import mario from '../assets/graphics/mario-atlas.png'
import Mario from '../gameObjects/Mario';
import tiles from '../assets/graphics/tiles.png';
import map1 from '../assets/map/tiles.json'
import boomba from '../gameObjects/Goomba'
import Goomba from '../gameObjects/Goomba';

/**
 * @description a logo scene example, this is the first scene to load
 * @author © Philippe Pereira 2020
 * @export
 * @class LogoScene
 * @extends {Scene}
 */
export default class LogoScene extends Scene
{
    cursors: Phaser.Types.Input.Keyboard.CursorKeys;
    mario: Phaser.GameObjects.Sprite;
    map: Phaser.Tilemaps.Tilemap;
    tileset: Phaser.Tilemaps.Tileset;
    collideLayer: any;
    goomba: Phaser.GameObjects.Sprite;
    
    constructor ()
    {
        super({
            key: 'logo'
        });
    }

    public preload (): void
    {
        // Preload assets needed for this scene and the loading scene
        
        this.load.image('logo', logo);
        this.load.spritesheet('mario', mario, {frameWidth: 16, frameHeight: 16});
        this.load.image('tiles', tiles);
        this.load.tilemapTiledJSON('map1', map1);
    }

    public create (): void
    {
        
        this.goomba = new Goomba(this, WIDTH /2, HEIGHT /2, 'mario', 11)
        this.mario = new Mario(this, WIDTH /4, HEIGHT /2, 'mario', 0)
        this.map = this.make.tilemap({ key: 'map1'});
        this.cursors = this.input.keyboard.createCursorKeys();
        
        this.tileset = this.map.addTilesetImage('tiles', 'tiles');
        this.collideLayer = this.map.createLayer('collides-calque', this.tileset, 0, 0).setDepth(0)
        this.collideLayer.setCollisionByProperty({collides: true});
        this.physics.world.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels)
        this.physics.add.collider(this.mario, this.collideLayer, undefined, undefined, this)
        this.physics.add.collider(this.goomba,this.collideLayer, undefined, undefined, this)
        this.cameras.main.startFollow(this.mario);
    }
    
    
}
